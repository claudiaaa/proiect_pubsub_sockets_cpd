import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;

public class Server  {
    public static String IP = "127.0.0.1";
    public static int[] PORTS = {1111, 2222, 3333};

    public void start() {

        for(int PORT: PORTS){
            new DebateServer(PORT).start();
        }

    }

    public static void main(String[] args) throws IOException {
        Server airBNBServer = new Server();
        airBNBServer.start();
    }
}
