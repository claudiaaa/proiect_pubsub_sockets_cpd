import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;


//gazde: stocare locatii + luni in care sunt disponibile
//clienti: rezerva o locatie pentru una sau mai multe luni
// avem orase si locatii in orase
// clientii pot sa interogheze dupa oras si sa rezerve orice locatie disponibila

public class DebateServer extends Thread {
  public static String IP = "127.0.0.1";
  public int PORT;
  private ServerSocket ss = null;
  private boolean token;
  private String tokenString;

  public DebateServer(int PORT){
    this.PORT = PORT;
  }

  public void run() {
    try {
      ss = new ServerSocket();
      ss.setReuseAddress(true);
      ss.bind(new InetSocketAddress(PORT));
      System.out.println("Debate Server is listening on port " + PORT);

      while (true) {
        Socket socketForClient = ss.accept();
        new DebateHandler(socketForClient, PORT).start();
        System.out.println("New client connected." + PORT);
      }
    }
    catch (IOException e) {
      System.out.println("Server exception: " + e.getMessage());
      e.printStackTrace();
    }
  }
}
