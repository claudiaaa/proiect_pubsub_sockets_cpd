import jdk.swing.interop.SwingInterOpUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DebateHandler extends Thread{
  private Socket socketForClient;
  private ObjectInputStream fromClient;
  private ObjectOutputStream toClient;
  public int port;
  private static int token = -1;

  DebateHandler(Socket socketForClient, int port) {
    this.socketForClient = socketForClient;
    this.port = port;

  }

  @Override
  public void run() {
    try {
      toClient = new ObjectOutputStream(socketForClient.getOutputStream());
      fromClient = new ObjectInputStream(socketForClient.getInputStream());
      System.out.println("Current thread: " + Thread.currentThread().getName());

      String operation_for_client_port = (String) fromClient.readObject();
      System.out.println(operation_for_client_port + " " + port);
      while(true) {

//        System.out.println(operation_for_client_port + port);
        //un thread a primit tokenul pe portul x, altul il va trimite inapoi prin portul x spre aplicatia vecina
        if(token==-1 && operation_for_client_port.equals("I write")) {
          System.out.println(operation_for_client_port + port);
          token = (int) fromClient.readObject();
          System.out.println("Received: " + token);
        }
        else{
          if(port == token && operation_for_client_port.equals("I read")){
            System.out.println("We need to send it");
            toClient.writeObject(token);
            token = -1;
            System.out.println("Sent");
          }
        }
        sleep(100);
      }
//      fromClient.close();
//      toClient.close();
//      socketForClient.close();

    } catch (IOException | ClassNotFoundException | InterruptedException ioException) {
//      System.out.println("Client has disconnected");
      ioException.printStackTrace();
    }
  }
}
