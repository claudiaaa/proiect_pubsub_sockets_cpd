import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Client {
    private Socket socketIn;
    private Socket socketOut;
    private ObjectInputStream in;
    private ObjectInputStream in2;
    private ObjectOutputStream out;
    private ObjectOutputStream out2;
    private int pIn;
    private int pOut;
    private boolean b_token;
    private int token;
    Gui gui;


    public Client(int rd, int wr, Gui gui){
        this.pIn = rd;
        this.pOut = wr;
        this.gui = gui;
    }

    public void connect(){
        try {
            socketOut = new Socket("127.0.0.1", pOut);
            socketIn = new Socket("127.0.0.1", pIn);
            out = new ObjectOutputStream(socketOut.getOutputStream());
            in2 = new ObjectInputStream(socketOut.getInputStream());
            out2 = new ObjectOutputStream(socketIn.getOutputStream());
            in = new ObjectInputStream(socketIn.getInputStream());
            System.out.println(pIn + "->" + pOut);
            out.writeObject("I write");
            out2.writeObject("I read");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void initDebate() {
        try{
            out.writeObject(pOut);
            System.out.println("Init Debate");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    // receive token, set b_token = true, that means the application has the token, sleep for 10 seconds
    // and then send the token to the next app
    // repeat
    public void handleToken(){

        Runnable runnable =
                () -> {
                    while(true){
                        try {
                            int t = (int) in.readObject();
                            token = t;
                            b_token = true;
                            gui.check_if_my_turn(b_token);
                            System.out.println("I received the token: " + token);
                            Thread.sleep(20000);
                            out.writeObject(pOut);
                            b_token = false;
                            gui.check_if_my_turn(b_token);
                        } catch (IOException | ClassNotFoundException | InterruptedException e) {
//                        e.printStackTrace();
                        }
                    }
                };

        Thread thread = new Thread(runnable);
        thread.start();

    }

    public void disconnect() throws IOException {
        in.close();
        out.close();
        socketIn.close();
        socketOut.close();
    }
}
