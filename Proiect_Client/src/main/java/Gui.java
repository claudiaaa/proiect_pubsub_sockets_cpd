import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

public class Gui {
    JFrame f;
    JLabel l_name = new JLabel();
    JLabel l_cooking = new JLabel("Cooking");
    JLabel l_music = new JLabel("Music");
    JLabel l_photography = new JLabel("Photography");
    JButton b_cooking=new JButton("Send");//creating instance of JButton
    JButton b_photography=new JButton("Send");//creating instance of JButton
    JButton b_music=new JButton("Send");//creating instance of JButton
    JTextField tf_cooking = new JTextField();
    JTextField tf_music = new JTextField();
    JTextField tf_photography = new JTextField();
    String name;
    JTextArea display_c = new JTextArea ( 16, 30 );
    JTextArea display_m = new JTextArea ( 16, 30 );
    JTextArea display_p = new JTextArea ( 16, 30 );
    private static final String EXCHANGE_NAME = "hobby_topics";
    ConnectionFactory factory = new ConnectionFactory();
    Connection connection;
    private String last_published_data = "";
    boolean cooking_topic=true;
    boolean music_topic=true;
    boolean photography_topic=true;

    public Gui(String name, String[] pub_topics, String[] sub_topics){
        f=new JFrame(name);//creating instance of JFrame
        l_name.setText(name);
        create_components();
        this.name = name;
        conf_connection();
        conf_pub_topics(pub_topics);
        subscribe(sub_topics);
        f.setSize(880,440);//400 width and 500 height
        f.setLayout(null);//using no layout managers
        f.setVisible(true);//making the frame visible
    }

    public void create_components(){
        display_c.setEditable ( false ); // set textArea non-editable
        display_c.setLineWrap(true);
        JScrollPane scroll_cooking = new JScrollPane ( display_c );
        scroll_cooking.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
        scroll_cooking.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        display_m.setEditable ( false ); // set textArea non-editable
        display_m.setLineWrap(true);
        JScrollPane scroll_music = new JScrollPane ( display_m );
        scroll_music.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
        scroll_music.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        display_p.setEditable ( false ); // set textArea non-editable
        display_p.setLineWrap(true);
        JScrollPane scroll_photography = new JScrollPane ( display_p );
        scroll_photography.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
        scroll_photography.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        l_name.setBounds(40, 15, 300, 20);

        l_cooking.setBounds(40,50, 300, 20);
        l_music.setBounds(310,50, 300, 20);
        l_photography.setBounds(580,50, 300, 20);

        b_cooking.setBounds(215,350,75, 30);
        b_music.setBounds(485,350,75, 30);
        b_photography.setBounds(755,350,75, 30);

        scroll_cooking.setBounds(40, 70, 250, 270);
        scroll_music.setBounds(310, 70, 250, 270);
        scroll_photography.setBounds(580, 70, 250, 270);

        tf_cooking.setBounds(40, 350, 175, 30);
        tf_music.setBounds(310, 350, 175, 30);
        tf_photography.setBounds(580, 350, 175, 30);

        f.add(l_name);//adding button in JFrame
        f.add(l_cooking);//adding button in JFrame
        f.add(l_music);//adding button in JFrame
        f.add(l_photography);//adding button in JFrame
        f.add(b_cooking);//adding button in JFrame
        f.add(b_music);//adding button in JFrame
        f.add(b_photography);//adding button in JFrame
        f.add(scroll_cooking);
        f.add(scroll_music);
        f.add(scroll_photography);
        f.add(tf_cooking);
        f.add(tf_music);
        f.add(tf_photography);

        b_cooking.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                handle_send("cooking");
            }
        } );
        b_music.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                handle_send("music");
            }
        } );
        b_photography.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                handle_send("photography");
            }
        } );
    }

    public void handle_send(String hobby){
        String topic;
        String message;
        topic = name + "." + hobby;
        switch(hobby){
            case "cooking":
                message = tf_cooking.getText();
                last_published_data = message;
                display_c.append("\t Publish: " + message + "\n");
                tf_cooking.setText("");
                break;
            case "music":
                message = tf_music.getText();
                last_published_data = message;
                display_m.append("\t Publish: " + message + "\n");
                tf_music.setText("");
                break;
            case "photography":
                message = tf_photography.getText();
                last_published_data = message;
                display_p.append("\t Publish: " + message + "\n");
                tf_photography.setText("");
                break;
            default:
                message = "Nothing";
        }

        publish(topic, message);

    }

    public void conf_connection(){
        factory.setHost("localhost");
    }

    public void conf_pub_topics(String[] pub_topics){

        if(!Arrays.asList(pub_topics).contains("cooking")){
            b_cooking.setEnabled(false);
            tf_cooking.setEditable(false);
            cooking_topic = false;
        }
        if(!Arrays.asList(pub_topics).contains("music")){
            b_music.setEnabled(false);
            tf_music.setEditable(false);
            music_topic = false;
        }
        if(!Arrays.asList(pub_topics).contains("photography")){
            b_photography.setEnabled(false);
            tf_photography.setEditable(false);
            photography_topic = false;
        }
    }

    public void publish(String routingKey, String message){
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(EXCHANGE_NAME, "topic");
            channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + routingKey + "':'" + message + "'");
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    public void subscribe(String[] sub_topics){
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = null;

            connection = factory.newConnection();

            Channel channel = connection.createChannel();

            channel.exchangeDeclare(EXCHANGE_NAME, "topic");
            String queueName = channel.queueDeclare().getQueue();

            if (sub_topics.length < 1) {
                System.err.println("Usage: ReceiveLogsTopic [binding_key]...");
                System.exit(1);
            }

            for (String bindingKey : sub_topics) {
                channel.queueBind(queueName, EXCHANGE_NAME, "*." + bindingKey);
            }

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                String topic = delivery.getEnvelope().getRoutingKey();
                System.out.println(" [x] Received '" + topic + "':'" + message + "'");
                String[] hobby = topic.split("\\.");
                System.out.println(hobby[1]);
                if((!message.equals(last_published_data))||(!hobby[0].equals(name))){

                    switch(hobby[1]){
                        case"cooking":
                            display_c.append("Received: " + message + "\n");
                            break;
                        case"music":
                            display_m.append("Received: " + message + "\n");
                            break;
                        case"photography":
                            display_p.append("Received: " + message + "\n");
                            break;
                    }

                }
            };
            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
        }
        catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void check_if_my_turn(boolean my_turn){
        if(my_turn){
            b_cooking.setBackground(Color.GREEN);
            b_music.setBackground(Color.GREEN);
            b_photography.setBackground(Color.GREEN);
            b_cooking.setEnabled(cooking_topic);
            tf_cooking.setEditable(cooking_topic);
            b_music.setEnabled(music_topic);
            tf_music.setEditable(music_topic);
            b_photography.setEnabled(photography_topic);
            tf_photography.setEditable(photography_topic);
        }
        else{
            b_cooking.setBackground(Color.RED);
            b_music.setBackground(Color.RED);
            b_photography.setBackground(Color.RED);
            b_cooking.setEnabled(false);
            tf_cooking.setEditable(false);
            b_music.setEnabled(false);
            tf_music.setEditable(false);
            b_photography.setEnabled(false);
            tf_photography.setEditable(false);
        }
    }


}