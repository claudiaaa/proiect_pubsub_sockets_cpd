import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class MainClient {
    public static void main(String[] args) throws IOException {

        String name;
        int in;
        int out;
        String p_topics;
        String s_topics;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name, input port, output port, coma separated publish topics, coma separated subscribe topics:");
        name = scanner.nextLine();
        in = scanner.nextInt();
        out = scanner.nextInt();
        scanner.nextLine();
        System.out.println(in+out);
        p_topics = scanner.nextLine();
        System.out.println(p_topics);
        s_topics = scanner.nextLine();
        System.out.println(s_topics);
        String[] pub_topics = p_topics.split(",");
        String[] sub_topics = s_topics.split(",");

        Gui gui = new Gui(name, pub_topics, sub_topics);


//        Runnable runnable =
//                () -> {
                    Client client = new Client(in, out, gui);
                    client.connect();
                    client.initDebate();
                    client.handleToken();

//                };
//        Thread thread = new Thread(runnable);
//        thread.start();

//        client.disconnect();
//        client2.disconnect();
//        client3.disconnect();

    }
}
